FROM python:3-slim

LABEL version="1"
LABEL authors="Marek Sirovy"
LABEL contact="msirovy@gmail.com"

# Copy application source to image
copy ./src/ /opt/cloudia-api

# Set application's home as workdir
WORKDIR /opt/cloudia-api

# Install deps
RUN pip install --upgrade pip && \
    pip install -r requirements.txt

# Set ENV
ENV PORT=5000
#ENV THREAD_COUNT=4

# Expose port
EXPOSE ${PORT}


# Entrypoint
#ENTRYPOINT ["/bin/bash"]
CMD ["/opt/cloudia-api/entrypoint.sh"]
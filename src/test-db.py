#!/usr/bin/env python

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from os import getenv
import socket
from contextlib import closing
 
def check_socket(host, port):
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        if sock.connect_ex((host, port)) == 0:
            return True
        else:
            return False


URI = getenv("DATABASE_URI", "cockroachdb://root@127.0.0.1:5432/claudia")

try:
    engine = create_engine(
        URI,
        connect_args={
            "application_name": "cloudia-api"
        }
    )

    db_session = sessionmaker(engine)

except Exception as e:
    print("Failed to connect to database.")
    print(f"{e}")

if __name__ == "__main__":

    if check_socket("localhost", 5432):
        print("Socket is available !!!")
    else:
        print("Socket is NOT available")

    print(f"Connecting {URI}...")
    
    q = 'SELECT 99'

    with db_session() as db:
        print("Run: ", q)
        db.execute(q)
        db.commit()
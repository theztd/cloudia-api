from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Server(Base):
    __tablename__ = "servers"

    fqdn = Column(String, primary_key=True, index=True)
    public_ip = Column(String)
    status = Column(String)

    def __repr__(self):
        return f"<Server(fqdn={self.fqdn}, public_ip='{self.public_ip}')>"

    def __str__(self):
        return f"{self.fqdn} ({self.public_ip})"
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from json import loads, dumps
from sqlalchemy import create_engine, exc
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import text
import pika

from os import getenv

import model
import schema

app = FastAPI()
app.debug = True
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


HOSTNAME = getenv("HOSTNAME")
DATABASE_URI = getenv("DATABASE_URI", "cockroachdb://root@127.0.0.1:26257/postgres")
TEMPO_ENDPOINT = getenv("TEMPO_ENDPOINT", "Undefined")
APP_NAME = f"cloudia-api--{HOSTNAME}"
AMQ_URL = getenv("RABBITMQ_HOST", "localhost")


def sendMessage(queue, data):
    # Init RabbitMQ aka amq
    amq_conn = pika.BlockingConnection(
        pika.ConnectionParameters()
    )
    amq = amq_conn.channel(AMQ_URL)
    amq.queue_declare(queue)
    amq.basic_publish(
        exchange="",
        routing_key=queue,
        body=str(data)
    )
    amq.close()
    return True


try:
    # Init engine with introducing
    engine = create_engine(
        DATABASE_URI,
        connect_args={
            "application_name": "cloudia-api",
#            "instance": HOSTNAME
        }
    )

    db_session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    
    # Test DB availability
    with db_session() as db:
        x = db.execute('SELECT 99')
        

except exc.OperationalError as e:
    print("ERR: Unable to connect database")
    print(e)


# Configure application observability & measurability
from utils import PrometheusMiddleware, metrics, setting_otlp


# Setting metrics middleware
app.add_middleware(PrometheusMiddleware, app_name=APP_NAME)
app.add_route("/metrics", metrics)

if TEMPO_ENDPOINT != "Undefined":
    # Setting OpenTelemetry exporter
    setting_otlp(app, APP_NAME, TEMPO_ENDPOINT)



@app.get("/v1/servers/")
def server_list():
    with db_session() as db:
        ret = db.query(model.Server).all()
    
    return ret

@app.post("/v1/servers/", response_model=schema.createServer)
def server_new(server: schema.createServer):
    print(server)
    new_srv = model.Server(
        fqdn=server.fqdn,
        public_ip=server.public_ip,
        status = ""
    )

    print(new_srv)

    try: 
        with db_session() as db:
            db.add(new_srv)
            db.commit()
            db.refresh(new_srv)
    except exc.IntegrityError as err:
        print(err)
        return {"msg": err}, 500

    
    return new_srv

@app.delete("/v1/servers/", response_model=schema.Server)
def server_delete(server: schema.Server):
    del_srv = model.Server(
        fqdn=server.fqdn
    )

    # TODO: doesn't work deleting
    with db_session() as db:
        for s2d in db.query(model.Server).filter(model.Server.fqdn == del_srv.fqdn).all():
            print(f"Delete server {s2d}")
            db.delete(s2d)
    
    return del_srv


@app.post("/v1/jobs/add", response_model=schema.createJob)
def jobAdd(data: schema.createJob):
    print(data)
    sendMessage(
        f"orders", 
        str(data.command)
    )
    return data



# Get version
try: 
    with open("VERSION", "r") as fv:
        VERSION = fv.read().strip()

except IOError as err:
    print(err)
    VERSION="missing"



# routes
@app.get("/status")
def get_status():
    return {"status": "ok", "version": VERSION}


@app.get("/", status_code=403)
def get_root():
    return {"msg": "Forbidden"}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="127.0.0.1", port=5000)
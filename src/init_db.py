#!/usr/bin/env python

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from os import getenv


try:
    engine = create_engine(
        getenv("DATABASE_URI", "cockroachdb://root@127.0.0.1:5432/claudia"),
        connect_args={
            "application_name": "cloudia-api"
        }
    )

    db_session = sessionmaker(engine)

except Exception as e:
    print("Failed to connect to database.")
    print(f"{e}")
    exit(99)





if __name__ == "__main__":
    queries = [
        """
            CREATE TABLE servers(  
                fqdn varchar(32) NOT NULL PRIMARY KEY,
                public_ip varchar(15),
                status varchar(15) DEFAULT 'unknown',
                description varchar(255)
            );
        """,
        "INSERT INTO servers (fqdn, public_ip) VALUES ('n1.fejk.net', '11.1.2.1');",
        "INSERT INTO servers (fqdn, public_ip) VALUES ('n2.fejk.net', '11.1.2.2');"
    ]

    with db_session() as db:
        for q in queries:
            print("Run: ", q)
            db.execute(q)
            db.commit()
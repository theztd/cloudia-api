#!/bin/bash -l

uvicorn main:app --port ${PORT:-5000} --host ${HOST:-0.0.0.0}
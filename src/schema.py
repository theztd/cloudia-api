from pydantic import BaseModel
from typing import Union

class Server(BaseModel):
    fqdn: str
    public_ip: Union[str, None] = ""
    # status: Union[str, None]

    class Config:
        orm_mode = True


class createServer(Server):
    fqdn: str



class createJob(BaseModel):
    fqdn: str
    command: str
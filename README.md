# Cloudia API

Cloudia is example project demonstrating development in the nomad cluster. Written in FastAPI (python)

## Description

API is the core service. Purpouse is:

- access to the DB
- access to the rabbitMQ
- auth (in future)


## Core functions API docs (Usage)

Full API documentation is on route **/docs** or **/redoc**


## Installation

Prefered way is using prebuild docker image. Or you can run it manualy following Devel description

## RUN

```bash
docker run -it --rm --name api \
    -p 8080:8080 \
    -e PORT=8080 \
    -e HOST=0.0.0.0 \
    claudie-api
```

## Devel

### Cocroach DB

```bash
docker run -it -p 26257:26257 -p 8080:8080 cockroachdb/cockroach:latest-v21.2 start-single-node --insecure
export DATABASE_URI="cockroachdb://root@127.0.0.1:26257/postgres"
```

### Postgresql

```bash
docker run --name postgres \
  -e POSTGRES_USER=cloudia \
  -e POSTGRES_PASSWORD=CloudiaPAssword123 \
  -e POSTGRES_DB=cloudia \
  -p 5432:5432 \
  -d postgres

export DATABASE_URI="postgresqldb://cloudia:CloudiaPAssword123@localhost:5432/cloudia"
```


```bash
# Initialize venv
python3 -m venv .venv

# Install project deps
./.venv/bin/pip3 install -r src/requirements.txt

# Activate venv
. ./.venv/bin/activate


# Run code
cd src
./entrypoint.sh
```

## Roadmap

### v1
  - list, create, delete server


### v1.1
  - Get versions from server via RMQ
  - List of tasks per server


### v1.2
  - auth and ACL


### v1.3
  - Add comments to server


### v1.4
  - Define MOTD per server
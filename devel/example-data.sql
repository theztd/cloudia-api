-- Active: 1664727454890@@127.0.0.1@5432@cloudia@public

CREATE TABLE servers(  
    fqdn varchar(32) NOT NULL PRIMARY KEY,
    public_ip varchar(15),
    status varchar(15) DEFAULT 'unknown',
    description varchar(255)
);

INSERT INTO servers (fqdn, public_ip) VALUES ('n1.fejk.net', '11.1.2.1');
INSERT INTO servers (fqdn, public_ip) VALUES ('n2.fejk.net', '11.1.2.2');
INSERT INTO servers (fqdn, public_ip) VALUES ('n3.fejk.net', '11.1.2.3');
INSERT INTO servers (fqdn, public_ip) VALUES ('n4.fejk.net', '11.1.2.4');
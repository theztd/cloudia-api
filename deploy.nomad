variable "fqdn" {
  type    = string
  default = "api-claudia.fejk.net"
}

variable "dcs" {
  type    = list(string)
  default = ["dc1", "devel"]
}

variable "image" {
  type = string
  default = "registry.gitlab.com/theztd/cloudia-api:00ed7ab3"
}

job "__JOB_NAME__" {
  datacenters = var.dcs

  group "backend" {
    count = 1

    network {
      port "api" { 
        to = 5000 
      }
    }

    service {
      name = "${JOB}-http"

      tags = [
        "public",
        "traefik.enable=true",
        "traefik.http.routers.${NOMAD_JOB_NAME}-api.rule=Host(`${var.fqdn}`)",
      ]

      port = "api"

      check {
        name     = "${NOMAD_JOB_NAME}-api - alive"
        type     = "http"
        //provider = "nomad"
        path     = "/status"
        interval = "1m"
        timeout  = "10s"

        # Task should run 2m after deployment
        check_restart {
          limit           = 5
          grace           = "2m"
          ignore_warnings = true
        }
      }
    }

    task "api" {
      driver = "docker"

      config {
        image      = var.image
        force_pull = true
        ports      = ["api"]
      }

      // ToDo: template finding service
      template {
            destination = "${NOMAD_SECRETS_DIR}/api.vars"
            env         = true
            change_mode = "restart"
            data        = <<EOF
{{- with nomadVar "nomad/jobs/cloudia-api" -}}
DATABASE_URI = "{{ .database_uri }}"
{{ end }}

{{ range nomadService "postgres-db" }}
PSQL_HOST= "{{ .Address }}: {{ .Port }}"
{{ end }}

EOF
// POSTGRES_PASSWORD = "{{.db_password}}"
// POSTGRES_USER = "{{.db_user}}"
// POSTGRES_DB = "{{.db_user}}"
      }

      env {
        PORT         = 5000
        HOST         = "0.0.0.0"
        THREAD_COUNT = 1
        RABBITMQ_URL = "localhost"
        DATABASE_URI = "postgresql://cloudia:CloudiaPAssword123@localhost:5432/cloudia"
      }

      resources {
        cpu        = 200
        memory     = 128
        memory_max = 128
      }

    }

  } # END group backend

}
